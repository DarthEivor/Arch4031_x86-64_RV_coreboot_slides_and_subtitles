﻿1
00:00:00,014 --> 00:00:04,055
Sometimes when you're doing development with QEMU, you might

2
00:00:04,056 --> 00:00:09,082
need to lock some output to file as

3
00:00:09,082 --> 00:00:13,084
you saw during the booting coreboot in QEMU

4
00:00:13,084 --> 00:00:17,096
everything running very, very fast and sometimes it is hard

5
00:00:17,097 --> 00:00:21,046
to catch all the things.

6
00:00:21,054 --> 00:00:25,045
Sometimes your terminal do not have enough scroll back,

7
00:00:25,045 --> 00:00:27,057
we do not have enough history,

8
00:00:27,058 --> 00:00:31,046
so it is also a good idea to have some

9
00:00:31,046 --> 00:00:37,035
additional options for logging to ???.

10
00:00:37,094 --> 00:00:42,056
There is such option inside coreboot built,

11
00:00:43,004 --> 00:00:47,022
you can see there is QEMU debug console output and

12
00:00:47,022 --> 00:00:49,032
there is IO address of this debug,

13
00:00:49,032 --> 00:00:53,036
console provided as parameter here.

14
00:00:54,094 --> 00:00:59,039
It should be already enabled by default but still,

15
00:00:59,039 --> 00:01:02,013
I would like to show you where it is in

16
00:01:02,014 --> 00:01:05,066
console mini, so you can remember

17
00:01:05,067 --> 00:01:07,096
how to find it.

18
00:01:08,074 --> 00:01:13,014
And then we will extend our CBQMO

19
00:01:13,014 --> 00:01:22,096
command with additional parameters which I will explain

20
00:01:23,074 --> 00:01:25,005
in a second.

21
00:01:25,006 --> 00:01:29,005
So, first of all let's switch to the console,

22
00:01:29,064 --> 00:01:33,077
we want to verify,

23
00:01:33,077 --> 00:01:38,076
where is the QEMU debug console set up.

24
00:01:39,014 --> 00:01:44,025
So, we're going to console and we're looking for you

25
00:01:44,025 --> 00:01:48,071
QEMU debug console, as you can see, here

26
00:01:48,071 --> 00:01:49,039
it is,

27
00:01:49,004 --> 00:01:54,004
you can change its and IO address but it's

28
00:01:54,004 --> 00:01:56,009
already said and should be compiled,

29
00:01:56,091 --> 00:02:02,085
in our coreboot of course everything is

30
00:02:02,085 --> 00:02:05,084
already compiled. So, no change

31
00:02:05,085 --> 00:02:08,026
that's why we didn't rebuild anything.

32
00:02:08,074 --> 00:02:11,046
So, let's get back to our slides.

33
00:02:13,094 --> 00:02:14,052
Okay,

34
00:02:14,052 --> 00:02:18,004
so you can see there is extension of the

35
00:02:18,041 --> 00:02:21,008
alias that we created QEMU with

36
00:02:21,081 --> 00:02:26,067
character device which will be attached to QEMU emulated

37
00:02:26,067 --> 00:02:27,036
system.

38
00:02:27,074 --> 00:02:36,076
This character device connects file and it will connect

39
00:02:37,024 --> 00:02:40,096
other device here identified by debugcon,

40
00:02:41,034 --> 00:02:44,084
the name is also here so it's good to remember,

41
00:02:44,085 --> 00:02:47,029
and the path to that file will be just local

42
00:02:47,029 --> 00:02:49,016
file with the name lock

43
00:02:49,016 --> 00:02:52,002
you can provide any name you want. And of course,

44
00:02:52,002 --> 00:02:53,092
we need to connect

45
00:02:53,093 --> 00:02:55,093
our ISA

46
00:02:55,094 --> 00:03:00,086
debug con device which based on the configuration of

47
00:03:00,086 --> 00:03:06,009
coreboot will be associated with IO address

48
00:03:06,009 --> 00:03:09,093
OX 402 This was defined in the car but previously

49
00:03:09,094 --> 00:03:12,077
and of course we just hook this address to ISA

50
00:03:12,077 --> 00:03:17,055
debug con and and then exposed that device to

51
00:03:17,056 --> 00:03:22,015
file. So, this file can be recorded to our disk.

52
00:03:22,054 --> 00:03:26,003
So let's check how good this command works.

53
00:03:29,014 --> 00:03:37,063
So, just let's use our alias, and now it should

54
00:03:37,063 --> 00:03:38,003
work.

55
00:03:38,004 --> 00:03:38,035


56
00:03:39,004 --> 00:03:39,087
Okay,

57
00:03:39,088 --> 00:03:43,091
so we already booted with C bios,

58
00:03:43,091 --> 00:03:46,009
the coreboot with the C bios in QEMU the output

59
00:03:46,009 --> 00:03:48,001
going to standard IO.

60
00:03:48,011 --> 00:03:53,045
When we leave, we should find the log file,

61
00:03:54,014 --> 00:03:55,069
of course there it is.

62
00:03:55,007 --> 00:03:59,006
And we will just show you top of that

63
00:03:59,006 --> 00:04:00,005
lock file,

64
00:04:00,051 --> 00:04:06,024
you can see we have coreboot booting here for 12

65
00:04:06,025 --> 00:04:07,037
boot block,

66
00:04:07,038 --> 00:04:10,096
just showing that everything is local level seven,

67
00:04:11,034 --> 00:04:14,025
and we're just processing the boot.

68
00:04:14,064 --> 00:04:16,005
So if we cut log,

69
00:04:16,051 --> 00:04:21,061
we will have to call log file of the

70
00:04:21,062 --> 00:04:22,025
system.

