﻿1
00:00:00,014 --> 00:00:01,096
So, let's practice with CBMEM.

2
00:00:02,064 --> 00:00:05,016
First step we can skip because we already did that.

3
00:00:05,074 --> 00:00:11,072
In second step, we look at the debian image partition

4
00:00:11,072 --> 00:00:17,026
layout to be able to calculate the offset of

5
00:00:17,026 --> 00:00:20,086
the loop bug device where the loop bug device will

6
00:00:20,086 --> 00:00:24,096
begin inside our image.

7
00:00:25,044 --> 00:00:30,049
In that way, we can despite our image contained

8
00:00:30,049 --> 00:00:31,053
multiple partitions.

9
00:00:31,053 --> 00:00:36,049
We can just mount one of the partitions. We

10
00:00:36,049 --> 00:00:38,069
create some directory,

11
00:00:38,007 --> 00:00:43,065
then we mount the loop device into a loop directory,

12
00:00:44,004 --> 00:00:47,014
and we can copy inside the image

13
00:00:47,015 --> 00:00:50,006
the compile CBMEM binary.

14
00:00:50,054 --> 00:00:54,016
We're just amount and detach all the loop bug

15
00:00:54,016 --> 00:01:00,028
devices. Then we can boot and actually see how CBMEM

16
00:01:00,028 --> 00:01:03,065
man works by gathering logs and like how we can

17
00:01:03,066 --> 00:01:05,045
extract those logs from the memory.

18
00:01:05,084 --> 00:01:10,026
So, let's try to use those instructions.

19
00:01:12,024 --> 00:01:13,007
First of all,

20
00:01:13,078 --> 00:01:20,015
let's use our Docker container,

21
00:01:21,064 --> 00:01:28,016
then we can simply check partition table,

22
00:01:34,004 --> 00:01:38,079
you can see that our first partition,

23
00:01:38,079 --> 00:01:45,086
which is our system starts at the block 2048

24
00:01:46,024 --> 00:01:51,001
and size of the sector is 512

25
00:01:51,001 --> 00:01:52,019
bytes.

26
00:01:52,023 --> 00:01:56,000
So just to calculate this

27
00:01:56,001 --> 00:02:02,056
512 gives us this number,

28
00:02:03,034 --> 00:02:08,017
of course we will use that number to create

29
00:02:08,017 --> 00:02:19,096
loop back device, and then we can create loop directory.

30
00:02:19,096 --> 00:02:21,028
It's already exists,

31
00:02:22,004 --> 00:02:23,075
let's check if nothing inside.

32
00:02:24,044 --> 00:02:30,026
That's more the develop 0 in loop directory.

33
00:02:31,054 --> 00:02:32,002


34
00:02:32,003 --> 00:02:33,076
So, I have type of here.

35
00:02:35,044 --> 00:02:35,009
Okay,

36
00:02:35,009 --> 00:02:36,038
in loop

37
00:02:36,038 --> 00:02:40,007
we have all the information of all the root file

38
00:02:40,007 --> 00:02:42,027
system of our debian.

39
00:02:42,028 --> 00:02:44,086
Let's copy the CBMEM binary,

40
00:02:45,024 --> 00:02:56,081
you can check, that's there

41
00:02:56,089 --> 00:02:58,006
we can see here.

42
00:02:59,004 --> 00:03:01,005
So, let's mount a loop,

43
00:03:04,034 --> 00:03:06,046
let's detach all the devices

44
00:03:07,004 --> 00:03:13,028
and now we can use our old alias to boot

45
00:03:13,029 --> 00:03:32,026
the system and run CBMEM.

46
00:03:32,026 --> 00:03:39,086
We just modified, in the same way

47
00:03:39,086 --> 00:03:45,046
you can use method for transferring all the files.

48
00:03:46,024 --> 00:03:46,056


49
00:03:47,084 --> 00:03:52,012
So, you can see we have CBMEM here. So

50
00:03:52,012 --> 00:03:55,089
we can create some fires inside this system and then

51
00:03:55,089 --> 00:04:00,031
mount the debian image and extract those either dumps,

52
00:04:00,031 --> 00:04:04,082
either some locks or of course you can use networking

53
00:04:04,082 --> 00:04:05,025
for that.

54
00:04:06,034 --> 00:04:09,004
So, let's check -C

55
00:04:09,004 --> 00:04:12,019
and you can see that we already-so -C

56
00:04:12,002 --> 00:04:21,023


57
00:04:21,024 --> 00:04:24,088
means that it will show us console.

58
00:04:24,089 --> 00:04:28,035
It's council lock which was gathered at

59
00:04:28,084 --> 00:04:30,052
during the boot time.

60
00:04:30,053 --> 00:04:35,015
Unless we have buffer in the

61
00:04:35,015 --> 00:04:39,007
memorial, and this buffer is controlled on the build time

62
00:04:39,007 --> 00:04:40,076
of the coreboot.

63
00:04:41,044 --> 00:04:46,009
We may have multiple boot, so that's why we have

64
00:04:46,009 --> 00:04:50,026
this -1 option, which gives us just last boot.

65
00:04:50,094 --> 00:04:57,001
And there is coverage information maybe, there's no coverage

66
00:04:57,001 --> 00:04:58,025
information included.

67
00:04:58,064 --> 00:05:02,008
There is list of tables which

68
00:05:02,008 --> 00:05:03,087
are included.

69
00:05:03,088 --> 00:05:07,014
There is quite a lot of them. So all

70
00:05:07,014 --> 00:05:11,051
this table can be presented.

71
00:05:11,052 --> 00:05:16,000
There are some TPM locks if there were

72
00:05:16,000 --> 00:05:21,041
gathered. There are some time stabs, unfortunately in case

73
00:05:21,041 --> 00:05:24,077
of QEMU, we will have problem of calculating time

74
00:05:24,078 --> 00:05:31,026
because there is not enough correct CPU frequency information in

75
00:05:31,026 --> 00:05:36,052
the system. But, we can use console and look what's

76
00:05:36,052 --> 00:05:38,089
going on here. And as you can see, we see

77
00:05:38,089 --> 00:05:44,045
boot block starting in the classical boot

78
00:05:44,045 --> 00:05:47,049
log from coreboot, then if there is ROM stage,

79
00:05:47,059 --> 00:05:55,064
and it continues booting with all the stages. And that's

80
00:05:55,064 --> 00:05:55,086
it.

