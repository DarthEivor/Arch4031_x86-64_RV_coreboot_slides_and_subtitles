﻿1
00:00:00,004 --> 00:00:00,035
Okay,

2
00:00:00,036 --> 00:00:03,081
now that you know how to configure some basic ???

3
00:00:03,081 --> 00:00:06,049
for QMU, let's go back for a while to the

4
00:00:06,005 --> 00:00:07,075
kconfig.

5
00:00:08,014 --> 00:00:11,033
So, when the ??? is configured,

6
00:00:11,034 --> 00:00:15,036
the kconfig produces two important output files containing the

7
00:00:15,037 --> 00:00:18,025
full set of options selected during the configuration phase.

8
00:00:18,074 --> 00:00:20,096
The first files is .config file,

9
00:00:21,034 --> 00:00:23,034
it is created and saved within the coreboot

10
00:00:23,034 --> 00:00:26,026
directory up on exciting from a new config

11
00:00:26,027 --> 00:00:27,076
text interface.

12
00:00:29,004 --> 00:00:33,006
The .config file gets included by the coreboot main

13
00:00:33,006 --> 00:00:33,056
make file.

14
00:00:34,034 --> 00:00:38,029
Its purpose is to expose the conflict options selected during

15
00:00:38,029 --> 00:00:40,094
the configuration phase to the make file so that the

16
00:00:40,094 --> 00:00:43,076
build system is aware of what is the target configuration.

17
00:00:44,034 --> 00:00:45,054
Based on this information,

18
00:00:45,054 --> 00:00:49,042
the build system knows which source files should be built,

19
00:00:49,048 --> 00:00:51,096
which features should be enabled, etc.

20
00:00:52,094 --> 00:00:55,015
As we can see in this short example,

21
00:00:55,016 --> 00:00:58,006
the ??? settings are set to yes in short why,

22
00:00:58,061 --> 00:01:02,007
or commented out with is not said string at

23
00:01:02,007 --> 00:01:03,016
the end.

24
00:01:04,004 --> 00:01:06,055
String settings can be set up and a string or

25
00:01:06,056 --> 00:01:07,096
basically left empty.

26
00:01:08,094 --> 00:01:14,032
The kconfig also allows to set various other options

27
00:01:14,032 --> 00:01:16,043
like various other types of options,

28
00:01:16,043 --> 00:01:18,086
like integer or hexadecimal.

29
00:01:20,004 --> 00:01:20,029
Okay,

30
00:01:20,029 --> 00:01:23,058
so now let's see quickly how it looks like for

31
00:01:23,058 --> 00:01:32,009
our ??? built and let's use the last command, as

32
00:01:32,009 --> 00:01:32,008
you can see,

33
00:01:32,081 --> 00:01:36,081
the header says that it is automatically generated file and

34
00:01:36,082 --> 00:01:38,064
it shouldn't be edited manually.

35
00:01:38,065 --> 00:01:41,007
So, if we want to make some modifications to do

36
00:01:41,007 --> 00:01:46,061
it for the many conflict and you can see the

37
00:01:46,062 --> 00:01:51,081
headers of various sections are named by the cermonies for

38
00:01:51,081 --> 00:01:52,017
example,

39
00:01:52,017 --> 00:01:55,019
we have the ??? setup and before we have

40
00:01:55,019 --> 00:01:59,081
the main board submini which is indicated by

41
00:01:59,081 --> 00:02:00,096
the main board header here.

42
00:02:02,024 --> 00:02:03,005
All right here,

43
00:02:03,084 --> 00:02:09,000
we have the general set up here, and

44
00:02:09,001 --> 00:02:11,076
let's study the few options we see.

45
00:02:12,014 --> 00:02:17,061
So, basically each kconfig option which is defined by

46
00:02:17,061 --> 00:02:19,045
by given name.

47
00:02:19,094 --> 00:02:25,078
It's always a punitive config_  here and the value

48
00:02:25,078 --> 00:02:30,037
set in the many configuration is assigned after the equal

49
00:02:30,037 --> 00:02:30,075
sign.

50
00:02:31,044 --> 00:02:34,078
So, basically this is a coreboot built boolean value which

51
00:02:34,078 --> 00:02:38,091
is set to, "yes" we have a strength local version

52
00:02:38,091 --> 00:02:42,013
which I showed, and it is configured as

53
00:02:42,013 --> 00:02:44,069
empty string, but here we have the CBFS

54
00:02:44,069 --> 00:02:49,042
perfix which indicates what kind of pefix should

55
00:02:49,043 --> 00:02:51,066
stages have.

56
00:02:52,095 --> 00:02:54,086
It is set to fall back string,

57
00:02:55,044 --> 00:03:00,019
you can also see other options like

58
00:03:00,019 --> 00:03:02,094
config any toolchain, which also has to use any other to change

59
00:03:02,094 --> 00:03:06,015
and coreboot one, but it is marked as not set.

60
00:03:07,004 --> 00:03:10,029
Let's try to find some other options like hexadecimal

61
00:03:10,029 --> 00:03:12,046
options or ??? options.

62
00:03:13,044 --> 00:03:16,062
So, for example here we have the CBFS size

63
00:03:16,062 --> 00:03:22,045
which is a hexadecimal value or we can have

64
00:03:23,022 --> 00:03:24,016
an ???.

65
00:03:24,017 --> 00:03:24,007
For example,

66
00:03:24,007 --> 00:03:27,046
the config for UART_FOR_CONSOLE is ???.

67
00:03:28,074 --> 00:03:29,000
Here,

68
00:03:29,000 --> 00:03:32,085
we can also make ACP table size in kilobytes it's

69
00:03:33,044 --> 00:03:40,006
224.

70
00:03:41,084 --> 00:03:45,052
That's probably are all types of options

71
00:03:45,052 --> 00:03:48,005
that a coreboot with implements and take on.

