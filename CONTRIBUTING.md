# Contribution

This file describe contribution procesures, tricks and useful snippets.

## Container rebuild in Ubuntu VM

```
$ sudo apt install -y build-essential
$ git clone https://gitlab.com/opensecuritytraining/arch4031_x86-64_rv_coreboot_code_for_class.git -b rel_4.13
$ cd arch4031_x86-64_rv_coreboot_code_for_class/util/docker
$ DOCKER_COMMIT=4.13 COREBOOT_IMAGE_TAG=4.13 COREBOOT_CROSSGCC_PARAM="build-i386 build_iasl" make coreboot-sdk
$ docker tag coreboot/coreboot-sdk:4.13 registry.gitlab.com/opensecuritytraining/arch4031_x86-64_rv_coreboot_code_for_class:4.13
```

## Code clocks formatting per line

```html
<p style="margin-top: 0px; margin-bottom: 0px;"><code>hello world</code></p>
```


## Images

```html
<img height="" width="" src="" alt="" />
```
