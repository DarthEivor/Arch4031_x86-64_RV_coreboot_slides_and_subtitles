﻿1
00:00:00,640 --> 00:00:02,150
Then we can ask ourselves,

2
00:00:02,150 --> 00:00:04,850
"Okay, so where is, where is firmware?"

3
00:00:04,850 --> 00:00:06,800
We know all these firmwares,

4
00:00:06,800 --> 00:00:08,450
all these terms,

5
00:00:08,450 --> 00:00:09,600
where this firmware is?

6
00:00:09,850 --> 00:00:11,450
To be honest,

7
00:00:11,450 --> 00:00:13,530
here on this picture we have

8
00:00:13,540 --> 00:00:18,000
a modern architecture of computer,

9
00:00:18,000 --> 00:00:20,800
we have two processors of Intel Haswell,

10
00:00:21,130 --> 00:00:24,100
we have PCI devices,

11
00:00:24,100 --> 00:00:26,150
we have many things here,

12
00:00:26,160 --> 00:00:30,240
we have southbridge or PCH

13
00:00:30,250 --> 00:00:31,220


14
00:00:31,230 --> 00:00:33,050
call it in these days,

15
00:00:33,060 --> 00:00:37,300
we have a BMC from ASPEED here,

16
00:00:37,550 --> 00:00:39,450


17
00:00:39,450 --> 00:00:41,500
and we ask ourselves, "Okay, where is firmware?"

18
00:00:41,500 --> 00:00:43,800
And it happens that firmware is everywhere

19
00:00:43,910 --> 00:00:45,250
So we have microcode,

20
00:00:45,250 --> 00:00:48,300
which is some kind of type of firmware which,

21
00:00:48,300 --> 00:00:52,150
which is inside CPU, and inside

22
00:00:52,150 --> 00:00:54,350
CPU chip there may be more

23
00:00:54,350 --> 00:00:57,250
other internal chips,

24
00:00:57,250 --> 00:00:58,750
which run some firmware.

25
00:00:59,310 --> 00:01:01,150
Then we have all these PCI devices,

26
00:01:01,150 --> 00:01:04,660
which by itself also can contain firmware.

27
00:01:04,850 --> 00:01:07,550
We have network controllers

28
00:01:07,550 --> 00:01:11,500
which also contain some firmware.

29
00:01:11,510 --> 00:01:12,650
Then we have PCH

30
00:01:12,650 --> 00:01:15,200
where we have something like Management Engine

31
00:01:15,200 --> 00:01:17,900
which also contains some form of firmware

32
00:01:17,900 --> 00:01:20,150
some RTOS which is running there.

33
00:01:20,800 --> 00:01:23,200
We have hard disks

34
00:01:23,200 --> 00:01:25,350
which contains some controllers,

35
00:01:25,350 --> 00:01:27,070
which contains some firmware.

36
00:01:27,080 --> 00:01:29,560
We have USB devices which by itself,

37
00:01:29,560 --> 00:01:32,250
to serve some functions containing firmware.

38
00:01:32,250 --> 00:01:34,040
We have TPM here,

39
00:01:34,050 --> 00:01:38,400
which also contains some firmware to provides

40
00:01:38,400 --> 00:01:39,760
provide its functions.

41
00:01:39,840 --> 00:01:41,120
We have BIOS,

42
00:01:41,130 --> 00:01:43,150
which is firmware in itself,

43
00:01:43,150 --> 00:01:45,150
which we will discuss in this training.

44
00:01:45,250 --> 00:01:47,700
We have OpenBMC, the same stuff,

45
00:01:47,700 --> 00:01:49,410
this is some form of firmware.

46
00:01:49,420 --> 00:01:52,430
So these days firmware is everywhere,

47
00:01:52,440 --> 00:01:53,800
and

48
00:01:53,800 --> 00:01:56,600
it's good to know what we're dealing with

49
00:01:56,600 --> 00:01:57,960
and learn about it.

