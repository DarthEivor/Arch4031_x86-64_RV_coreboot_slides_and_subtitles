﻿1
00:00:01,199 --> 00:00:07,200
As

2
00:00:03,040 --> 00:00:10,480
coreboot developers we very frequently

3
00:00:07,200 --> 00:00:11,360
work on long projects on complex

4
00:00:10,480 --> 00:00:13,440
projects

5
00:00:11,360 --> 00:00:15,599
and it is very important to have good

6
00:00:13,440 --> 00:00:17,840
documentation related to

7
00:00:15,599 --> 00:00:19,039
designs, concepts, assumptions,

8
00:00:17,840 --> 00:00:22,400
stipulations,

9
00:00:19,039 --> 00:00:24,400
and like any problems debugging sessions

10
00:00:22,400 --> 00:00:26,960
and this kind of stuff, because

11
00:00:24,400 --> 00:00:28,560
the problems are typically very complex

12
00:00:26,960 --> 00:00:30,880
and it's good to have

13
00:00:28,560 --> 00:00:31,599
your own way of documenting this complex

14
00:00:30,880 --> 00:00:33,360
process.

15
00:00:31,599 --> 00:00:36,160
So, if you will switch contacts you will

16
00:00:33,360 --> 00:00:39,680
get back after a couple days you can

17
00:00:36,160 --> 00:00:41,760
quickly continue where you stopped.

18
00:00:39,680 --> 00:00:43,280
So, developer documentation is very

19
00:00:41,760 --> 00:00:46,480
important,

20
00:00:43,280 --> 00:00:48,800
you should document any bugs you find

21
00:00:46,480 --> 00:00:50,000
because like from one perspective, it may

22
00:00:48,800 --> 00:00:53,120
bite you

23
00:00:50,000 --> 00:00:54,800
later, from other perspective

24
00:00:53,120 --> 00:00:57,440
it would be great if you can contribute

25
00:00:54,800 --> 00:01:00,079
fixes to coreboot project.

26
00:00:57,440 --> 00:01:01,199
Definitely, rule is quality over

27
00:01:00,079 --> 00:01:05,199
quantity.

28
00:01:01,199 --> 00:01:08,560
So, please try to be concise

29
00:01:05,199 --> 00:01:10,720
and do not

30
00:01:08,560 --> 00:01:12,000
write big elaborates because this

31
00:01:10,720 --> 00:01:13,840
doesn't help

32
00:01:12,000 --> 00:01:15,280
anyone, nobody has time to read

33
00:01:13,840 --> 00:01:19,200
documentation.

34
00:01:15,280 --> 00:01:20,799
Focus on on the target,

35
00:01:19,200 --> 00:01:22,240
so if you're writing documentation

36
00:01:20,799 --> 00:01:24,799
please think for

37
00:01:22,240 --> 00:01:26,000
for who you're writing. So, for example if

38
00:01:24,799 --> 00:01:28,799
you were write something

39
00:01:26,000 --> 00:01:29,439
for doc coreboot.org it typically can

40
00:01:28,799 --> 00:01:34,240
be

41
00:01:29,439 --> 00:01:37,840
coreboot hacker, coreboot,

42
00:01:34,240 --> 00:01:38,479
expert it may be some user. So, please try

43
00:01:37,840 --> 00:01:42,399


44
00:01:38,479 --> 00:01:45,280
more user-friendly,

45
00:01:42,399 --> 00:01:46,560
do not use too much of complex

46
00:01:45,280 --> 00:01:49,680
terminology.

47
00:01:46,560 --> 00:01:51,600
Also if if this is for vendors

48
00:01:49,680 --> 00:01:54,079
this also have to have different

49
00:01:51,600 --> 00:01:54,720
perspective and for developers you can

50
00:01:54,079 --> 00:01:58,399
use

51
00:01:54,720 --> 00:02:01,040
all the style

52
00:01:58,399 --> 00:02:03,040
that is already in the source code in

53
00:02:01,040 --> 00:02:06,560
the comments.

54
00:02:03,040 --> 00:02:07,280
So, in terms of doc.coreboot.org

55
00:02:06,560 --> 00:02:10,879


56
00:02:07,280 --> 00:02:12,080
this is something that replaced the

57
00:02:10,879 --> 00:02:15,440
previous

58
00:02:12,080 --> 00:02:16,400
wiki, and so wiki is considered right now

59
00:02:15,440 --> 00:02:18,879
deprecated

60
00:02:16,400 --> 00:02:22,000
but still contains a lot of valuable

61
00:02:18,879 --> 00:02:25,040
information which was created over last

62
00:02:22,000 --> 00:02:28,160
I don't know 10+ years.

63
00:02:25,040 --> 00:02:31,200
But, now coreboot migrates to sphinx

64
00:02:28,160 --> 00:02:34,000
and start to use doc.coreboot.org for

65
00:02:31,200 --> 00:02:35,280
all the documentation. It's much more

66
00:02:34,000 --> 00:02:38,480
friendly,

67
00:02:35,280 --> 00:02:41,760
we

68
00:02:38,480 --> 00:02:46,400
suggest to read that and to support

69
00:02:41,760 --> 00:02:46,400
if you find any bugs there.

