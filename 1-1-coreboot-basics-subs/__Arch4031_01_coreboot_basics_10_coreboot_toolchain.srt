﻿1
00:00:00,044 --> 00:00:05,097
So, let's talk about coreboot toolchain. So, coreboot project use

2
00:00:05,097 --> 00:00:12,002
very specific toolchain with exact version of software of GCC

3
00:00:12,002 --> 00:00:15,006
of libraries of GDB,

4
00:00:15,094 --> 00:00:19,053
and this toolchain is built as a part

5
00:00:19,054 --> 00:00:20,086
of the project.

6
00:00:21,074 --> 00:00:26,004
A coreboot project maintains couple patches on top of toolchain

7
00:00:26,004 --> 00:00:27,016
components.

8
00:00:28,014 --> 00:00:29,062
But to be honest,

9
00:00:29,062 --> 00:00:30,066
like you may ask,

10
00:00:30,067 --> 00:00:31,019
okay,

11
00:00:31,002 --> 00:00:33,008
but on in our distribution,

12
00:00:33,008 --> 00:00:34,041
I can install GCC,

13
00:00:34,041 --> 00:00:34,056
GDB

14
00:00:34,056 --> 00:00:34,071


15
00:00:34,071 --> 00:00:35,032


16
00:00:35,033 --> 00:00:36,009
and I have Lipsey,

17
00:00:36,091 --> 00:00:39,066
so why I need another toolchain?

18
00:00:40,087 --> 00:00:42,047
This is because first of all,

19
00:00:42,047 --> 00:00:44,019
coreboot project want to be

20
00:00:44,019 --> 00:00:44,096
independent,

21
00:00:45,034 --> 00:00:47,009
and it happened in the past

22
00:00:47,001 --> 00:00:48,045
many times there are values

23
00:00:48,045 --> 00:00:56,029
Distributions provide broken toolchains or incompatible toolchains. People

24
00:00:56,029 --> 00:00:57,075
that create toolchains

25
00:00:57,076 --> 00:00:59,003
in general,

26
00:00:59,003 --> 00:01:03,011
proposed distributions, make a lot of assumptions about how to

27
00:01:03,011 --> 00:01:04,045
chain will be used,

28
00:01:04,046 --> 00:01:08,046
and typically they do not consider open source firmware development

29
00:01:08,046 --> 00:01:10,026
as a main use case.

30
00:01:10,034 --> 00:01:16,006
They typically creator change for developing under the their distribution.

31
00:01:16,094 --> 00:01:17,082
Also,

32
00:01:17,083 --> 00:01:18,088


33
00:01:18,089 --> 00:01:23,043
coreboot project want to give ability to be built on

34
00:01:23,043 --> 00:01:25,027
values or essence,

35
00:01:25,028 --> 00:01:26,087
or SS and distrust,

36
00:01:26,088 --> 00:01:30,005
including BSD or or sequins.

37
00:01:30,064 --> 00:01:32,008
Also,

38
00:01:32,009 --> 00:01:35,019
coreboot supports multiple CPU architectures,

39
00:01:35,019 --> 00:01:41,006
what may be different in comparison to approach of

40
00:01:41,061 --> 00:01:46,091
toolchain developed and distributed by Linux distrust, or values

41
00:01:46,092 --> 00:01:48,036
other providers.

42
00:01:49,014 --> 00:01:54,009
And because of that coreboot gives ability to build

43
00:01:54,009 --> 00:01:56,088
toolchain, it's mandatory by the way,

44
00:01:56,089 --> 00:01:57,037


45
00:01:57,038 --> 00:02:00,011
it requires quite a lot of time

46
00:02:00,012 --> 00:02:01,065
and because of that,

47
00:02:01,066 --> 00:02:04,014
we use Docker to deliver the

48
00:02:04,015 --> 00:02:07,067
this toolchain,

49
00:02:07,068 --> 00:02:08,069


50
00:02:08,007 --> 00:02:12,076
and not wait hours for building the toolchain on our

51
00:02:13,004 --> 00:02:14,006
computer.

52
00:02:15,004 --> 00:02:17,006
So if you will run,

53
00:02:17,006 --> 00:02:19,099
make with help,

54
00:02:20,000 --> 00:02:21,005
then you will see,

55
00:02:21,051 --> 00:02:22,001


56
00:02:22,011 --> 00:02:25,005
a couple ways of building toolchain, you might build

57
00:02:25,005 --> 00:02:27,016
for all CPU architectures,

58
00:02:27,016 --> 00:02:30,043
you might be built only for 32 bits for 64

59
00:02:30,043 --> 00:02:32,006
bits for power,

60
00:02:32,006 --> 00:02:33,046
for risk.

61
00:02:34,015 --> 00:02:35,015
It depends

62
00:02:35,015 --> 00:02:36,066
what is your target.

63
00:02:37,004 --> 00:02:38,046
This procedure,

64
00:02:38,046 --> 00:02:38,094
of course,

65
00:02:38,094 --> 00:02:40,046
is very time consuming,

66
00:02:40,046 --> 00:02:42,085
it may fail because of various reasons

67
00:02:42,086 --> 00:02:43,082


68
00:02:43,083 --> 00:02:46,066
those toolchains are tested by corporate projects,

69
00:02:46,066 --> 00:02:50,093
but sometimes it happened that there are box, because someone

70
00:02:50,094 --> 00:02:52,085
didn't use that for a long time.

71
00:02:53,044 --> 00:02:53,074
And

72
00:02:53,074 --> 00:02:54,018
of course,

73
00:02:54,018 --> 00:03:00,059
to proceed with that, best would be to

74
00:03:00,059 --> 00:03:04,016
use Docker container that we deliver,

75
00:03:04,016 --> 00:03:07,054
which will be presented on further slides.

76
00:03:07,055 --> 00:03:08,009
But first of all,

77
00:03:08,009 --> 00:03:12,045
you should have Docker install it and ready to use

78
00:03:12,046 --> 00:03:13,022
of course,

79
00:03:13,023 --> 00:03:14,056
in previous practice

80
00:03:14,074 --> 00:03:15,066


81
00:03:16,074 --> 00:03:17,048
exercise

82
00:03:17,048 --> 00:03:18,073
we already use Docker,

83
00:03:18,073 --> 00:03:22,056
so I believe everything is in the place and it should

84
00:03:22,056 --> 00:03:23,006
work.

