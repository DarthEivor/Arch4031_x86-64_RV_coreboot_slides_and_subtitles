﻿1
00:00:00,044 --> 00:00:03,065
Let's talk a little bit about contribution to coreboot project.

2
00:00:04,034 --> 00:00:05,053
On this slide,

3
00:00:05,053 --> 00:00:08,096
you see the screenshot of review.coreboot.org

4
00:00:09,034 --> 00:00:14,087
This is the website where all the patches that were

5
00:00:14,087 --> 00:00:17,075
made by coreboot developers are managed.

6
00:00:18,014 --> 00:00:20,035
It's based on Grrrit

7
00:00:21,054 --> 00:00:24,001
to participate in discussion,

8
00:00:24,001 --> 00:00:27,023
you have to create account and there are multiple ways

9
00:00:27,023 --> 00:00:31,045
to sign up to the system.

10
00:00:32,034 --> 00:00:34,004
And you can see that there is

11
00:00:34,004 --> 00:00:35,085
like nice user interface,

12
00:00:35,086 --> 00:00:43,055
which least all contribution from various developers. Protest for

13
00:00:43,055 --> 00:00:46,021
contribution is very simple,

14
00:00:46,022 --> 00:00:46,097
first of all,

15
00:00:46,097 --> 00:00:48,038
you create this account,

16
00:00:48,039 --> 00:00:50,008
then you clone repository,

17
00:00:50,009 --> 00:00:50,091
you

18
00:00:50,092 --> 00:00:52,049
check out some branch,

19
00:00:52,005 --> 00:00:55,036
then you modify cold, test this code,

20
00:00:55,037 --> 00:00:56,034


21
00:00:56,035 --> 00:00:59,017
then when you have something that works,

22
00:00:59,018 --> 00:01:01,076
you should commit code in

23
00:01:01,094 --> 00:01:02,062


24
00:01:02,063 --> 00:01:03,051
certain way

25
00:01:03,051 --> 00:01:06,076
I will describe in in the next slide,

26
00:01:07,043 --> 00:01:11,003
and then you should clean this code and make

27
00:01:11,003 --> 00:01:16,033
sure that it's a according to coreboot project guidelines.

28
00:01:16,036 --> 00:01:21,083
That's all the indentation or the coding style

29
00:01:21,083 --> 00:01:26,035
rules are applied as well as description of the changes

30
00:01:26,036 --> 00:01:31,053
tests and related stuff are in place. When everything he's

31
00:01:31,053 --> 00:01:33,000
layout correctly,

32
00:01:33,001 --> 00:01:35,046
then you can push this upstream.

